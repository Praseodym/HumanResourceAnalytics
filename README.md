# Human Resource Analytics

HR analytics or workforce analytic an important field that involves collecting and analyzing data about employees to improve business outcomes. At this project, we show three business cases where statistics can help to make data driven decision and detect anomalies:

- Sickness notifications in a company
- Employee Satisfaction
- Job Post Anomalies 

